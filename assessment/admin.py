from django.contrib import admin
from .models import Topic, Content, Question, QuestionType, QuestionDifficulty, Answer, User, AssessmentTest, AssessmentTestMain, UserTestScores

# defining admin for models


class AnswerInLine(admin.TabularInline):
  model = Answer
  extra = 1


class QuestionAdmin(admin.ModelAdmin):
  model = Question
  list_display = ('text', 'difficulty', 'question_type')
  inlines = [AnswerInLine]


class QuestionInLine(admin.StackedInline):
  model = Question
  extra = 1


class AssessmentTestAdmin(admin.ModelAdmin):
  list_display = ('test_id', 'test_question')
  search_fields = ['test_id__name', 'test_question']


class AssessmentTestMainAdmin(admin.ModelAdmin):
  list_display = ('name', 'test_user',  'date_created', 'creator')
  search_fields = ['name', 'test_user__first_name', 'test_user__last_name', 'creator']


class UserTestScoresAdmin(admin.ModelAdmin):
  list_display = ('user', 'test_id', 'test_score')



# Register your models here.
admin.site.register(Topic)
admin.site.register(Content)
admin.site.register(Question, QuestionAdmin)
admin.site.register(QuestionType)
admin.site.register(QuestionDifficulty)
admin.site.register(Answer)
admin.site.register(User)
admin.site.register(UserTestScores, UserTestScoresAdmin)
admin.site.register(AssessmentTestMain, AssessmentTestMainAdmin)
admin.site.register(AssessmentTest, AssessmentTestAdmin)

