from .models import Question, Answer

class ScoreCalculator():
    """ Class to encapsulate score calculation logic for different question types"""

    def __init__(self, answer):
        #answer can be one answer or a list:
        if(type(answer) == list):
            self.answer = [ Answer.objects.get(pk=a) for a in answer]
            self.question = self.answer[0].question
        else:
            self.answer = Answer.objects.get(pk=answer)
            self.question = self.answer.question
        self.qType = self.question.question_type.name

    def calculate(self):
    # maximum score for each question of any type is 1, 
    # for multiselects you can have partially correct score

        if(self.qType == "YesNo"):
            return int(self.answer.correct)
    
        elif(self.qType == "MultiSelect"):
           #answer is a list
            totalCorrectAnswers = len([a for a in self.question.answer_set.all() if a.correct])
            correctAnswers = 0
            for a in self.answer:
                if(a.correct):
                    correctAnswers += 1
                else:
                    correctAnswers -= 1
            return correctAnswers/totalCorrectAnswers

        elif(self.qType == "LongForm"):
            correctAnswer = self.question.answer_set.all()
            if(self.answer.lower() == correctAnswer.text.lower()):
                return 1
            else:
                return 0

        elif(self.qType == "Numerical"):
            correctAnswer = self.question.answer_set.all()
            if(str(self.answer).lower() == correctAnswer.text.lower()):
                return 1
            else:
                return 0

        else:
            return 0
