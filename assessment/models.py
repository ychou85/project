from django.db import models

# Create your models here.




class Topic(models.Model):
  name = models.CharField(max_length=255)

  def __unicode__(self):
    return self.name




class Content(models.Model):
  name = models.CharField(max_length=255, db_index=True)
  content_type = models.PositiveSmallIntegerField(db_index=True)
  payload = models.TextField()
  topic = models.ForeignKey(Topic, on_delete=models.CASCADE)

  def __unicode__(self):
    return self.name

  class Meta:
    verbose_name_plural = 'Content'




class QuestionType(models.Model):
  name = models.CharField(max_length=255)

  def __unicode__(self):
    return self.name




class QuestionDifficulty(models.Model):
  name = models.CharField(max_length=255)
  
  class Meta:
    verbose_name_plural = 'Question Difficulty'

  def __unicode__(self):
    return self.name




class Question(models.Model):
  text = models.TextField()
  difficulty = models.ForeignKey(QuestionDifficulty)
  question_type = models.ForeignKey(QuestionType)

  def __unicode__(self):
    return self.text




class Answer(models.Model):
  text = models.TextField()
  question = models.ForeignKey(Question, on_delete=models.CASCADE)
  correct = models.BooleanField()

  def __unicode__(self):
    return self.text




class User(models.Model):
  handle = models.CharField(max_length=255, unique=True)
  email = models.CharField(max_length=255, unique=True)
  first_name = models.CharField(max_length=255)
  last_name = models.CharField(max_length=255)

  def __unicode__(self):
    return self.first_name + " " + self.last_name



class AssessmentTestMain(models.Model):
  name = models.CharField(max_length=255)
  creator = models.CharField(max_length=255)
  date_created = models.DateTimeField('date created', auto_now_add=True)
  test_user = models.ForeignKey(User, on_delete=models.CASCADE)
  test_content = models.ForeignKey(Content)

  def __unicode__(self):
    return self.name

  class Meta:
    verbose_name_plural = 'Assessment Test Main'
  



class AssessmentTest(models.Model):
  test_id = models.ForeignKey(AssessmentTestMain, on_delete=models.CASCADE)
  test_question = models.ForeignKey(Question)

  def __unicode__(self):
    return "Test " + str(self.test_id)
  



class UserTestScores(models.Model):
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  test_id = models.ForeignKey(AssessmentTestMain, on_delete=models.CASCADE)
  test_score = models.PositiveIntegerField(blank=True, null=True)

  def __unicode__(self):
    return "Score on test "+ str(self.test_id) + " for user id" + str(self.user)

  class Meta:
    verbose_name_plural = 'User Test Scores'
  
