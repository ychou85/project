# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('correct', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='AssessmentTest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('test_id', models.PositiveIntegerField()),
                ('creator', models.CharField(max_length=255)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name=b'date created')),
                ('test_score', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, db_index=True)),
                ('content_type', models.PositiveSmallIntegerField(db_index=True)),
                ('payload', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='QuestionDifficulty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='QuestionType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('handle', models.CharField(unique=True, max_length=255)),
                ('email', models.CharField(unique=True, max_length=255)),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='question',
            name='difficulty',
            field=models.ForeignKey(to='assessment.QuestionDifficulty', choices=[(0, 0), (1, 1), (2, 2)]),
        ),
        migrations.AddField(
            model_name='question',
            name='question_type',
            field=models.ForeignKey(to='assessment.QuestionType', choices=[(0, 0), (1, 1), (2, 2)]),
        ),
        migrations.AddField(
            model_name='content',
            name='topic',
            field=models.ForeignKey(to='assessment.Topic'),
        ),
        migrations.AddField(
            model_name='assessmenttest',
            name='test_content',
            field=models.ForeignKey(to='assessment.Content'),
        ),
        migrations.AddField(
            model_name='assessmenttest',
            name='test_question',
            field=models.ForeignKey(to='assessment.Question'),
        ),
        migrations.AddField(
            model_name='assessmenttest',
            name='test_user',
            field=models.ForeignKey(to='assessment.User'),
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(to='assessment.Question'),
        ),
    ]
