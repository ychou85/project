from django import forms
from .models import Question, Answer

class AnswerCreator():
    """ Helper class to dynamically compose the answer fields"""
    def __init__(self, questions):
        self.questions = questions

    def makeFields(self):
        count = 1
        fields = [] #list of tuples (fieldName, fieldType)
        for q in self.questions:
            answersToQ = q.answer_set.all()
            tuplify = [(a.id,a.text) for a in answersToQ]
            fieldName = 'Question ' + str(count)
            fieldSelect = None
            if(q.question_type.name == "YesNo"):
                fieldSelect = forms.ChoiceField(widget=forms.RadioSelect(),choices=tuplify,required=True, label='Question: ' + q.text)
            elif(q.question_type.name == "MultiSelect"):
                fieldSelect = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(), required=True, choices=tuplify, label='Question: ' + q.text)
            elif(q.question_type.name == "Numerical"):
                fieldSelect = forms.IntegerField(required=True, label='Question: ' + q.text)
            elif(q.question_type.name == "LongForm"):
                fieldSelect = forms.CharField(required=True, label='Question: ' + q.text)
            else:
                fieldSelect = "BadQuestionType"

            
            if(fieldSelect):
                fields.append((fieldName, fieldSelect))
            count += 1

        return fields



        
class TestForm(forms.Form):
    """form with all the relevant questions and answers"""
    def __init__(self, *args, **kwargs):
        questions = kwargs.pop('questions')
        super(TestForm, self).__init__(*args, **kwargs)
        formFieldList = AnswerCreator(questions).makeFields()
        
         #add field values to form
        for pair in formFieldList:
            #If question type is bad don't show that question but render the rest of the questions
            if(pair[1] != "BadQuestionType"):
                self.fields[pair[0]] = pair[1]
