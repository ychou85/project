from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/lessons/$', views.LessonView.as_view(), name='lessons'),
    url(r'^lessons/(?P<pk>[0-9]+)/tests/$', views.TestView.as_view(), name='testlist'),
    url(r'^testdetail/(?P<pk>[0-9]+)/$', views.TestDetailView.as_view(), name='testdetail'),
    url(r'^testresult/(?P<pk>[0-9]+)/(?P<user_id>[0-9]+)/$', views.calculateTestResult, name='testresult'),

]
