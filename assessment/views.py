from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.views import generic
from .forms import TestForm
from .scoreCalculator import ScoreCalculator

from .models import Topic, Content, Question, Answer, AssessmentTest, AssessmentTestMain, User, UserTestScores, QuestionDifficulty, QuestionType

# Create your views here.

class IndexView(generic.ListView):
    """index view of topics"""
    template_name = 'assessment/index.html'
    context_object_name = 'topics_list'

    def get_queryset(self):
        """Return the topics"""
        return Topic.objects.order_by('name')


class LessonView(generic.ListView):
    """view for list of lessons"""
    template_name = 'assessment/lessons.html'
    context_object_name = 'lessons_list'

    def get_context_data(self, **kwargs):
      context = super(LessonView, self).get_context_data(**kwargs)
      context['topic'] = Topic.objects.get(pk=self.kwargs.get('pk',None)) 
      return context

    def get_queryset(self):
        """Return lessons of this topic"""
        return Content.objects.filter(topic_id=self.kwargs.get('pk',None)).order_by('name')


class TestView(generic.ListView):
     """view that has content and list of available tests"""
     template_name = 'assessment/testslist.html'
     context_object_name = 'tests_list'

     def get_context_data(self, **kwargs):
       context = super(TestView, self).get_context_data(**kwargs)
       contentObject = Content.objects.get(pk=self.kwargs.get('pk', None))
       context['content_payload'] = contentObject
       return context

     def get_queryset(self):
         """Return tests associated with this content"""
         contentObject = Content.objects.get(pk=self.kwargs.get('pk',None))
         if(contentObject):
           return contentObject.assessmenttestmain_set.all()



class TestDetailView(generic.ListView):
     """view with content and questions for one user test"""
     template_name = 'assessment/testdetail.html'
     context_object_name = 'questions_list'

     def createForm(self):
       error_dict = self.kwargs.get('error_dict')
       if(error_dict):
           self.form = TestForm(questions=self.test_questions)
           self.form.errors = error_dict
       else:
           self.form = TestForm(questions=self.test_questions)

        
     def get_context_data(self, **kwargs):
       context = super(TestDetailView, self).get_context_data(**kwargs)
       self.createForm()
       context['content_payload'] = self.testMainObject.test_content
       context['test_name'] = self.testMainObject.name
       context['test_id'] = self.testMainObject.id
       context['test_user'] = self.testMainObject.test_user.id
       context['form'] = self.form
       return context

     def get_queryset(self):
         """Return questions associated with this test"""
         self.testMainObject = AssessmentTestMain.objects.get(pk=self.kwargs.get('pk'))
         self.question_answer_list = self.testMainObject.assessmenttest_set.all()
         self.test_questions = [ t.test_question for t in self.question_answer_list]
         return self.test_questions




def calculateTestResult(request, pk , user_id ):
     """function that calcs and saves score then renders template"""
     if request.method != 'POST':
         return HttpResponseNotFound('<h1>Bad Form Method</h>')

     mainTest = get_object_or_404(AssessmentTestMain, pk=pk)
     user = get_object_or_404(User, pk=user_id)

     testItems = mainTest.assessmenttest_set.all()
     questions = [t.test_question for t in testItems]

     # bind form data:
     form = TestForm(request.POST,questions=questions)
     # check for validity:
     if form.is_valid():
         formResults = form.cleaned_data
         #answer_id_selections = [v for v in formResults if(k.find('Question',0,len('Question')) != -1)]
         totalNumQuestions = len(questions)
         # count correct answers
         correct = 0
         for fieldname,value in formResults.iteritems():
             currentAnswer = value
             correct+=ScoreCalculator(currentAnswer).calculate()
         numScore = None
         if(totalNumQuestions):
             numScore = int(correct*100/totalNumQuestions)

         # add to database or update
         newScore, created = UserTestScores.objects.update_or_create(user=user, test_id=mainTest,test_score=numScore)
         newScore.save()
         #render score page
         return render(request, 'assessment/testresult.html', {'test_name': mainTest.name, 'score': numScore})
     else: #form not valid
         return render(request, 'assessment/testdetail.html', {'test_id':pk, 'test_user':user_id, 'pk':pk, 'form':form, 'content_payload':mainTest.test_content, 'test_name':mainTest.name})





