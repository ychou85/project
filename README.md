# README #

Some general design choices for the project are described at a high level below:

1) Please see the Database Schema pdf I sent to Danielle Razzano for Jace to get a quick idea of how I organized the django models. The high level gist is that we have users, questions, content (for the questions), answers, tests, and user scores. 

- Users can browse a list of topics which has a list of "content" subtopics. Each subtopic contains a list of tests which contain lists of questions. Each test is slated for one user only and only that user should be able to use that test (not implemented this way see below). Each test is a form and each question has a list of answers the user can select.

- Questions can be associated with any "content" for reusability. This allows you to perhaps create tests on the same content of varying degrees of difficulty, or a mix of hard and easy questions for one subtopic, without having to store duplicate questions in the database.

- However, answers are strictly associated with one and only one question. This is to allow easy calculation of test scores, but sacrifices database space since we now have redundant answers in the Answers table.

- Ideally the web site will be user based with login and cookies tracking, but since that stuff takes time and is rather generic to implement I skipped it for now. You can see the user based structure built in to the model and the views though.

2) I went ahead and designed the form creation logic in an OO way so as to be easily extensible for different types of questions. I have for now implemented four types (tested 2 types, the multiselect and single select). You can see this in the file form.py.

3) Typical error checking and error display for the form is implemented to display errors if for example a user does not select any answers.

4) Score is calculated, with multiselect questions scored based on how many (correct choices - incorrect choices)/(correct choices), so it is possible to get a negative score, similar to how the SAT is scored (at least back when I took it).

Please let me know if Danielle does not send you the pdf document. Thanks! ychou85@gmail.com
